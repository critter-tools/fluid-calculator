# Fluid Therapy Calculator

Warning: This site is still being developed.
It should not be used in the planning of fluid therapy.

#

This calculator is not a replacement for training or a medical device or diagnostic tool,
and only intended as a guide for permitted and trained carers.

- ALL wildlife coming into care will have some degree of dehydration.

- Animals still in shock MUST NOT be given fluids.

- Animals MUST be warm before attempting subcutaneous injections.

- Fluid therapy should not be attempted without approved training and associated veterinary treatment.

<br>

## Contact
critter.tools@gmail.com

<br>

# Formulae

```
[mL]: milli-litres = litres / 1000
[g]:  gram
[kg]: kilo-gram = g x 1000
[hr]: hour(s)
[%]:  percentage (0 to 100)
```

```
Fluid Deficit [mL] = Weight [g] x Dehydration [%]
```

```
Hourly Maintenance Dose [mL/hr] =
    Weight [kg] x Normal Fluid Usage [mL/kg/hr]
```

```
Total Fluid [mL] = 
    Hourly Maintenance Dose [mL/hr] x Rehydration Timeframe [hr]
  + Fluid Deficit [mL]
```

```
Daily Maintenance Dose =
    24 [hr] x Hourly Maintenance Dose [mL/hr]
```
# 

```
Initial Subcut Dose [mL] = 0.05 x Weight [g]

Remaining Fluid [mL] =
    Total Fluid [mL] - Initial Subcut Dose [mL]
```
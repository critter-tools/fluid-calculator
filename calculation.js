(function() {
  // utilities
  var box, btn, clear_input, copy_timeframe, daily_maintenance, daily_maintenance_dose, deficit, dehydration, dehydration_label, field, fluid_deficit, fluid_needs, fluid_needs_label, fluid_per_hour, get_by_id, highlight_change, hourly_maintenance_dose, i, initial_fluid_label, inputs, j, k, len, len1, len2, on_click, on_focusout, on_input, output_boxes, print, remaining_fluid, reset_anim, restore_input, round, setter, str, stringify, subcut, subcut_dose, timeframe, timeframe_copy, timeframe_label, timing, to_grams, to_kg, total_fluid, total_fluid_dose, total_remaining_fluid, update_outputs, weight, weight_conversions, weight_grams_btn, weight_kg_btn, weight_label, weight_mg_btn, weight_unit_btns, weight_unit_click, weight_units;

  get_by_id = function(id) {
    return document.getElementById(id);
  };

  on_click = function(obj, func) {
    return obj.addEventListener('click', func);
  };

  on_input = function(obj, func) {
    return obj.addEventListener('input', func);
  };

  on_focusout = function(obj, func) {
    return obj.addEventListener('focusout', func);
  };

  round = function(number, n_decimals = 0) {
    var scale;
    scale = 10 ** n_decimals;
    return Math.round((number + Number.EPSILON) * scale) / scale;
  };

  str = function(obj, trim = true) {
    return String(obj).trim();
  };

  setter = function(element, attr, val) {
    return element.setAttribute(attr, val);
  };

  print = function(obj) {
    return console.log(obj);
  };

  // inputs
  timeframe = get_by_id('timeframe-hours');

  weight = get_by_id('weight');

  dehydration = get_by_id('dehydration');

  fluid_needs = get_by_id('fluid-needs');

  timing = get_by_id('timing');

  inputs = [weight, dehydration, fluid_needs, timeframe];

  // labels
  weight_label = get_by_id('weight-label');

  dehydration_label = get_by_id('dehydration-label');

  fluid_needs_label = get_by_id('fluid-usage-label');

  timeframe_label = get_by_id('timeframe-label');

  initial_fluid_label = get_by_id('initial-fluid-label');

  // buttons
  weight_kg_btn = get_by_id('weight-kg-button');

  weight_grams_btn = get_by_id('weight-grams-button');

  weight_mg_btn = get_by_id('weight-mg-button');

  weight_unit_btns = [weight_kg_btn, weight_grams_btn, weight_mg_btn];

  // outputs
  total_fluid = get_by_id('total-fluid');

  daily_maintenance = get_by_id('daily-maintenance');

  deficit = get_by_id('deficit');

  subcut = get_by_id('subcut');

  remaining_fluid = get_by_id('remaining-fluid');

  output_boxes = [total_fluid.parentElement, daily_maintenance.parentElement, deficit.parentElement, subcut.parentElement, remaining_fluid.parentElement];

  // other
  timeframe_copy = get_by_id('timeframe-copy');

  // weight units
  weight_units = 'kg';

  weight_conversions = {
    'kg-grams': 1e3,
    'kg-mg': 1e6,
    'grams-kg': 1e-3,
    'grams-mg': 1e3,
    'mg-kg': 1e-6,
    'mg-grams': 1e-3
  };

  weight_unit_click = function(event) {
    var btn, button, conversion, i, len, new_units;
    button = event.target;
    new_units = str(button.textContent);
    for (i = 0, len = weight_unit_btns.length; i < len; i++) {
      btn = weight_unit_btns[i];
      btn.disabled = btn === button ? true : false;
    }
    conversion = `${str(weight_units)}-${new_units}`;
    weight.value = weight.value * weight_conversions[conversion];
    return weight_units = new_units;
  };

  for (i = 0, len = weight_unit_btns.length; i < len; i++) {
    btn = weight_unit_btns[i];
    on_click(btn, weight_unit_click);
  }

  // conversions
  to_kg = function(mass) {
    var conversion;
    if (weight_units !== 'kg') {
      conversion = `${str(weight_units)}-kg`;
      mass = mass * weight_conversions[conversion];
    }
    return mass;
  };

  to_grams = function(mass) {
    var conversion;
    if (weight_units !== 'grams') {
      conversion = `${str(weight_units)}-grams`;
      mass = mass * weight_conversions[conversion];
    }
    return mass;
  };

  // calculations
  fluid_deficit = function() {
    var deficit_ml, weight_grams;
    weight_grams = to_grams(weight.value);
    deficit_ml = weight_grams * dehydration.value / 100;
    return deficit_ml;
  };

  hourly_maintenance_dose = function() {
    var weight_kg;
    weight_kg = to_kg(weight.value);
    return weight_kg * fluid_needs.value;
  };

  total_fluid_dose = function() {
    var deficit_ml, maintenance;
    maintenance = timeframe.value * hourly_maintenance_dose();
    deficit_ml = fluid_deficit();
    return maintenance + deficit_ml;
  };

  fluid_per_hour = function() {
    return total_fluid_dose() / timeframe.value;
  };

  daily_maintenance_dose = function() {
    return hourly_maintenance_dose() * 24;
  };

  subcut_dose = function() {
    return 0.05 * to_grams(weight.value);
  };

  total_remaining_fluid = function() {
    return total_fluid_dose() - subcut_dose();
  };

  // updating
  stringify = function(number, n_digits = 4) {
    return str(number.toString().slice(0, n_digits + 1));
  };

  highlight_change = function(element, new_val) {
    var box, new_txt;
    new_txt = stringify(new_val);
    if (str(element.textContent) !== new_txt) {
      box = element.parentElement;
      if (box.className === 'ouput-box') {
        box.classList.add('pulse');
      } else {
        box.classList.add('fade');
      }
      return element.textContent = new_txt;
    }
  };

  reset_anim = function(event) {
    event.target.classList.remove('pulse');
    return event.target.classList.remove('fade');
  };

  for (j = 0, len1 = output_boxes.length; j < len1; j++) {
    box = output_boxes[j];
    box.addEventListener('animationend', reset_anim);
  }

  copy_timeframe = function() {
    return timeframe_copy.textContent = `(over ${timeframe.value} hours)`;
  };

  update_outputs = function(event) {
    event.target._prev_value = event.target.value;
    highlight_change(total_fluid, total_fluid_dose());
    highlight_change(daily_maintenance, daily_maintenance_dose());
    highlight_change(deficit, fluid_deficit());
    highlight_change(subcut, subcut_dose());
    highlight_change(remaining_fluid, total_remaining_fluid());
    copy_timeframe();
  };

  // input
  clear_input = function(event) {
    var target;
    target = event.target;
    target._prev_value = target.value;
    target.value = "";
  };

  restore_input = function(event) {
    var target;
    target = event.target;
    target.value = target._prev_value;
  };

  for (k = 0, len2 = inputs.length; k < len2; k++) {
    field = inputs[k];
    on_click(field, clear_input);
    on_input(field, update_outputs);
    on_focusout(field, restore_input);
  }

}).call(this);


//# sourceMappingURL=calculation.js.map
//# sourceURL=coffeescript